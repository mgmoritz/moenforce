# What it does

Schedules the execution of an action. Rings a bell before execution.

# Install

```elisp
(use-package moenforce
  :if (file-directory-p "/path/to/lib/moenforce")
  :load-path "/path/to/lib/moenforce")
```

# Usage

## Schedule an action in future

This function does nothing if the scheduled time is in the past.

```elisp
(moenforce-add-action-if-future "read-emails-morning"
                                "08:00"
                                'browse-url
                                "http://gmail.com")
```

## Remove an scheduled action

Call interactively `moenforce-remove-future-action`. Select the schedule action to delete from a list.

# Why not just use run-at-time

I prefer using this package instead of `run-at-time` to conveniently keep the timers
and allow me to delete them when needed.


# Reference

The starting point of the development was the idea contained in the alarm-clock functions
in [Nachopp's Blog post](https://ignaciopp.wordpress.com/2009/07/09/roosters-crow-setting-up-an-alarm-clock-in-emacs/ "Nachopp's Blog").
