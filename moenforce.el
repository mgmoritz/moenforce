;;; moenforce.el --- Enforce scheduled actions

;; Copyright (C) 2011 Free Software Foundation, Inc.

;; Author: Marcos G Moritz <marcosmoritz@gmail.com>
;; Version: 0.0.1
;; Package-Requires: ((helm "20201019.715"))
;; Keywords: run-at-time
;; URL: https://mo-profile.com

;;; Commentary:
;; This is package helps on the creation of scheduled actions

;; This is package allows you to schedule function executions
;; it is written on top of run-at-time and were inspired by
;; alarm-clock functions in Nachopp's Blog. See readme for
;; details.


(require 'timer)
(require 'diary-lib)
(require 'helm)

(defvar moenforce--items '())

;; public interface
(defun moenforce-add-action (name time func &rest func-args)
  "Set an agenda action."
  (setq moenforce--items
        (append moenforce--items
                `((,name . ,(apply 'run-at-time
                                   (if (and (not (car func-args)) (= 1 (length func-args)))
                                       `(,time ,nil moenforce--action ,func)
                                     (append `(,time ,nil moenforce--action ,func)
                                             func-args))))))))

(defun moenforce-remove-future-action ()
  "Remove an action of the enforced agenda items"
  (interactive)
  (let* ((items moenforce--items)
         (remove-future-action-helm-source
          `((name . "Select the item you want to remove: ")
            (candidates . ,(mapcar (lambda (item)
                                     `(,(car item) . ,item))
                                   items))
            (action . (lambda (candidate)
                        (moenforce--remove-future-action (car candidate)))))))
    (helm :sources '(remove-future-action-helm-source))))

;; private functions
(defun moenforce--action (&rest args)
  "Agenda action"
  (progn
    (let((i 0))
      (while (< i 1)
        (play-sound-file "/usr/share/sounds/speech-dispatcher/test.wav")
        (setq i (1+ i)))
      (moenforce--remove-past-actions)
      (apply (car args) (cdr args)))))

(defun moenforce--remove-past-actions ()
  (let ((items moenforce--items)
        (result '()))
    (while items
      (let ((item (car items)))
        (if (time-less-p (current-time) (timer--time (cdr item)))
            (setq result (append result `(,item))))
        (setq items (cdr items))))
    (setq moenforce--items result)))

(defun moenforce--remove-future-action (name)
  (let ((result '())
        (itens moenforce--items))
    (while itens
      (let ((item (car itens)))
        (if (not (string= (car item) name))
            (setq result (append `(,item) result))
          (cancel-timer (cdr item))))
      (setq itens (cdr itens)))
    (setq moenforce--items result)))

(defun moenforce-add-action-if-future (name time callback &optional cbargs)
  (if (time-less-p (current-time) (moenforce--string-to-time time))
      (moenforce-add-action name
                            time
                            callback
                            cbargs)))

(defun moenforce--get-future-action-names ()
  (mapcar 'car moenforce--items))

(defun moenforce--string-to-time (time-string)
  (let ((hhmm (diary-entry-time time-string))
        (now (decode-time)))
    (if (>= hhmm 0)
        (encode-time 0 (% hhmm 100) (/ hhmm 100) (nth 3 now)
                     (nth 4 now) (nth 5 now) (nth 8 now)))))

(provide 'moenforce)
